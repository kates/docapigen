var buster = require("buster");
var parser = require("../lib/parser").parser;
var fs = require('fs');


buster.testCase("Parser", {
	"comment block 1": function() {
		var code = "/**\n * @name Klass\n * @param num\n * @param num2\n*/\nfunction Klass(num,num2){}";
		var ast = parser.parse(code);

		assert.equals(ast[0]['comment']['tags'].length, 3);
		assert.equals(ast[0]['comment']['tags'][0]['tag'], 'name');
		assert.equals(ast[0]['comment']['tags'][0]['value'], 'Klass');
		assert.equals(ast[0]['comment']['tags'][1]['tag'], 'param');
		assert.equals(ast[0]['comment']['tags'][1]['value'], 'num');
		assert.equals(ast[0]['comment']['tags'][2]['tag'], 'param');
		assert.equals(ast[0]['comment']['tags'][2]['value'], 'num2');

		assert.equals(ast[0]['function']['name'], 'Klass');
		assert.equals(ast[0]['function']['args'].length, 2);
		assert.equals(ast[0]['function']['args'][0], 'num');
		assert.equals(ast[0]['function']['args'][1], 'num2');
	},
	"comment block 2": function() {
		var code = "/**\n * @name Klass\n *\n * @param num\n * @param num2\n*/\nfunction Klass(num, num2){}";
		var ast = parser.parse(code);
		//console.log(ast[0]['function'])
		assert.equals(ast[0]['comment']['tags'].length, 3);
		assert.equals(ast[0]['comment']['tags'][0]['tag'], 'name');
		assert.equals(ast[0]['comment']['tags'][0]['value'], 'Klass');
		assert.equals(ast[0]['comment']['tags'][1]['tag'], 'param');
		assert.equals(ast[0]['comment']['tags'][1]['value'], 'num');
		assert.equals(ast[0]['comment']['tags'][2]['tag'], 'param');
		assert.equals(ast[0]['comment']['tags'][2]['value'], 'num2');

		assert.equals(ast[0]['function']['name'], 'Klass');
		assert.equals(ast[0]['function']['args'].length, 2);
		assert.equals(ast[0]['function']['args'][0], 'num');
		assert.equals(ast[0]['function']['args'][1], 'num2');
	}
});