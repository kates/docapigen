start
	= doc*

doc
	= whitespace* "/**" whitespace* body:body? whitespace* "*/" whitespace* func:functionBody { return {comment: body, function:func};}

body
	= lines:line* { var tags = []; for (var i=0; i<lines.length; i++){ if (lines[i] != '') { tags.push(lines[i]);}} return {tags: tags};}

line
	= " "* "*" whitespace+ comment:comment? { return comment;}

comment
	= "@" tag:ident " "+ value:ident whitespace+ { return {tag: tag, value: value};}

functionBody
	= "function" " "+ name:ident "(" args:arglist* ")" "{" "}" whitespace* { return {name: name, args:args};}

arglist
	= arg:ident ("," " "*)? { return arg;}

whitespace
	= " "
	/ "\n"

ident
	= chars:[0-9a-zA-Z]+ { return chars.join('');}