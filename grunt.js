module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-buster');

	grunt.initConfig({
		buster: {
			test: {
				config: "test/buster.js"
			}
		}
	});
};
